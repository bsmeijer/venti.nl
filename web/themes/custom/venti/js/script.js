(function ($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.mainMenu = {
        attach: function (context) {
            $('.main-menu-button').once().click(function () {
                if ($('body').hasClass('main-menu-open')) {
                    $('body').removeClass('main-menu-open');
                }
                else {
                    $('body').addClass('main-menu-open');
                }
            });

            $('.nav-blinder').click(function () {
                $('body').removeClass('main-menu-open');
            });
        }
    };

    Drupal.behaviors.frontpageCarousel = {
        attach: function (context) {
            $('.view-frontpage .view-content').once().slick({
                arrows: false,
                dots: true
            });
        }
    };

})(jQuery, Drupal, drupalSettings);
